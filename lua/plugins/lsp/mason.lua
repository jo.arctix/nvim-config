return {
  -- Automatically install LSPs to stdpath for neovim
  'williamboman/mason.nvim',
  'williamboman/mason-lspconfig.nvim',
}
