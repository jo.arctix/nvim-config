return {
  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',
  "blazkowolf/gruber-darker.nvim",
  "xiyaowong/transparent.nvim",
}
