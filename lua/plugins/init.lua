local autocomplete  = require("plugins.autocomplete")
local explorer      = require("plugins.explorer")
local git           = require("plugins.git")
local lsp           = require("plugins.lsp")
local style         = require("plugins.style")
local utility       = require("plugins.utility")

return {
  "folke/which-key.nvim",
  autocomplete,
  explorer,
  git,
  lsp,
  style,
  utility,
}
